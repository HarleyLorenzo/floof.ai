# Floof.ai

A discord bot written in Python3 (using the discord.py) library all centered around our favorite DELTARUNE character, Ralsei!

## Project Wiki
[Project Wiki](https://gitlab.com/HarleyLorenzo/floof.ai/wikis/home)

## Getting Started

### Prerequisites

#### Dependencies
```
python 3.5, 3.6
```

```
discord.py >=1.0.0
```
Discord.py rewrite above can be acquired by:
```
python3 -m pip -U install https://github.com/Rapptz/discord.py@rewrite#egg=discord.py[voice]

```
#### Installation
To install floof.ai, simply run

```
python setup.py install
```

in the root directory of the project. This will install an entry point called ralsai that you can then run using

```
floofai
```

#### Config
##### Token
Floof.ai depends on a token for the bot to run. You can acquire a token through discord's developer portal. You can specify a token through the command line with --token or a file that holds the token with --tokenfile. Otherwise the bot will attempt to find the token at either /etc/floofai/tokenfile or in its current working directory. If these don't work, it will ask for one in the console.

##### system.d Service
Floof.ai includes a system.d service as cfg/systemd/floofai.service. You can install this in /etc/systemd/system/ if you want the systemd service, but as of right now, the service file isn't installed automatically with setup.py.

***NOTE:*** Make sure to setup file permissions appropriately. Just a heads up so you don't introduce a security hazard ;)

## Versioning
Floof.ai uses [Semantic Versioning](https://semver.org/)

## Authors
See AUTHORS

## Contributing
See CONTRIBUTING.md

## License
See LICENSE

## Code of Conduct
See CODE_OF_CONDUCT.md

## Donating
If you would like to support the project and its server costs you can either donate to Harley A.W. Lorenzo on Ko-fi or through bitcoin!

[Ko-fi](https://ko-fi.com/T6T1Q4NN)

Bitcoin Address: 1NFPKPivsSSrR9WZgq5qoPSPMfY3tdfB76

Every little bit helps Floof.ai grow and also ensures its server costs are covered!
