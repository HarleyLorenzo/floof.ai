FROM ubuntu:latest
WORKDIR /floofai/
RUN apt update && apt full-upgrade -y
RUN apt install -y python3 python3-pip
RUN python3 -m pip install discord.py
COPY . .
RUN python3 setup.py install
RUN mkdir /etc/floofai/ && cp ./tokenfile /etc/floofai/tokenfile
RUN cp ./cfg/systemd/floofai.service /etc/systemd/system/
RUN rm -R ./*
CMD ["floofai"]
