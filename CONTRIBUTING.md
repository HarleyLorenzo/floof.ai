# Contributing to Floof.ai

## Getting Started
 * Create a GitLab account
 * Talk with the team in our [Discord server](https://discord.gg/Sm8fV3E) in #floof-ai-development and learn what you can do to help!

## Code of Conduct
 * By contributing in any way to our project, you agree to follow our CODE OF CONDUCT (See CODE_OF_CONDUCT.md)

## Submitting Bug Reports/Issues
 * Create a GitLab account
 * On our [issues page](https://gitlab.com/HarleyLorenzo/floof.ai/issues), double check that your bug/issue hasn't already been reported, and if it has, comment on the issue with any helpful information (see below in **What To Report** for tips on what to include! 
 * If it's not on there, make a new issue with a clear title and description and look at our guide below **What To Report** for helpful information you can include!

### What To Report
 * How long have you been having the issue and what version does it happen on?
 * What happens and what do you think should happen?
 * Can you replicate the issue? If so, how?

## Contributing Code
 * Although we are repeating a point, we absolutely recommend you go to our [Discord server](https://discord.gg/Sm8fV3E) and go to #floof-ai-development and learn about our team, our current goals, and how we contribute code! We will answer all questions and even help those who have never coded before
 * Once you think your code is ready, make a merge request with our master branch and a maintainer will review it and if all is well and good it will be accepted!

### Project Development Wiki
[Wiki Here](https://gitlab.com/HarleyLorenzo/floof.ai/wikis/Development/Development)

## Testing Code
You will need to install the project in order to test code due to the module imports. You can use setup.py's --develop argument to specifically tell it you are developing features and it will automatically update the library that it uses to the source files in the directory, allowing for extremely quick code review and changes.
