### This is the entry point for the entire bot that contains on-run code
### As well as the core of the bot itself

### Imports

# Python Standard Library
import os
import sys
import argparse
import time
import asyncio
from pathlib import Path

# Discord.py Library
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config

### Functions

def file_openable_test(directory_to_test):
	file_to_test = Path(directory_to_test)
	return file_to_test.is_file()

### Async
async def rich_presence():
	while True:
		await config.client.change_presence(activity=discord.Game
			(name='Hugging those that use f.help'))
		await asyncio.sleep(30)

### Bot setup

# Perform after conection actions 
@config.client.event
async def on_ready():
	asyncio.ensure_future(rich_presence())
	# Output to stdout that the bot is ready!
	print('Floof.ai is ready!')
	print('Floof.ai version: {}'.format(config.version))
	print('Python version: {}.{}.{}'.format(*sys.version_info))
	print('Bot name#num: {}'.format(config.client.user))
	print('Bot clientid: {}'.format(config.client.user.id))

# The entry point for Floof.ai
def main():

	# Parse args passed to rals.ai

	# Start the argument parser
	parser = argparse.ArgumentParser(prog='Floof.ai')
	
	# Arguments to be added
	
	# --token, -t
	parser.add_argument('--token', '-t', 
		help = 'Specify a token directly', 
		action = 'store', 
		dest = 'token')
	# --tokenfile
	parser.add_argument('--tokenfile', 
		help = 'Specify a tokenfile location',
		action = 'store', 
		dest = 'tokenfile')
	# --version
	parser.add_argument('-version',
		help = 'Displays the version of Floof.ai',
		action = 'store_true',
		dest = 'version')

	# Begin the parsing
	args = parser.parse_args()

	# Here we obtain a token for discord 
	#TODO: In this entire section, add checks for empty tokenfiles

	#TODO: In the following section, make use of path.join()
	# If a config was specified
	if args.version:
		print(config.version)
		sys.exit(0)
	elif args.token:
		token = args.token
	elif args.tokenfile:
		# Check if we can open it
		directory_to_test = args.tokenfile
		if file_openable_test(directory_to_test):
			# If we can, then read the output into token
			token_file = open(directory_to_test,'r')
			token = token_file.read()
		else:
			# If we can't,
			# drop text into stdout and exit with an error
			sys.stderr.write(
				'Config file provided could not be opened!\n')
			sys.exit(-1)
	else:
		# This is the varriable that we use to house where we're testing
		directory_to_test = '/etc/floofai/tokenfile' 
		# If one is found in /etc/rals.ai/tokenfile, proceed as normal
		if file_openable_test(directory_to_test):
			# We set token_file to open it
			token_file = open(directory_to_test,'r')
			# Then we read the string into token
			token = token_file.read()

		# If there is not a tokenfile in /etc/rals.ai/tokenfile, 
		# look in where the script is located
		else:
			# This gets the folder where our script is located
			directory_to_test = os.path.dirname(
				os.path.realpath(__file__))
			# We add /tokenfile to it to actually look for the file
			directory_to_test = directory_to_test + '/tokenfile'
	
			if file_openable_test(directory_to_test):
				# We set token_file to open it
				token_file = open(directory_to_test,'r')
				# Then we read the string into token
				token = token_file.read()
			# If nothing else works 
			# simply ask for one in the terminal
			else:
				token = input('No tokenfile detected. ' \
					'Please input a token now: ')	 

	# All of our extensions/cogs that we need to import
	init_extensions = [
		'floofai.extensions.development',
		'floofai.extensions.music',
		'floofai.extensions.ralsei',
		'floofai.extensions.silly',
		'floofai.extensions.utilities',
	]

	# Import the extensions and error if we can't
	for extension in init_extensions:
		try:
			config.client.load_extension(extension)
			print('Successfully loaded extension: {}'.format(extension))
		except Exception as e:
			print('Failed to load the extension: {}\n{}'.format(extension, e),
				file=sys.stderr)

	# Actually run the bot with the token from earlier, stripping it of 
	# unnecessary characters, specifically newlines
	config.client.run(token.strip())

# If executed normally, run the main function (which starts the bot)
if __name__ == '__main__':
		main()
