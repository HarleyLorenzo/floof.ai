# Emotes.py contains anything related to server emotes and their use

# Floof.ai Modules
from floofai import config

class EmotesList():
	'Stores server ID and emote string values in tuples for getEmote to use'
	# Church of Ralsei emotes
	FloofGlad			= (507647360760020993, 'FloofGlad')
	ralseiBlush 		= (507647360760020993, 'RalseiBlush')
	ralseiThink 		= (507647360760020993, 'RalseiThink')
	ralseiLove	 		= (507647360760020993, 'RalseiLove')
	# Harley's personal server emotes
	harleyBooperSnoot 	= (346069868644401154, 'harleyBOOPERSNOOT')

def getEmote(emote):
	'Gets an emotes string value each time a command needs it'
	# Get a discord server object and check if it's None
	# and if it is, return an empty string for the emote
	server = config.client.get_guild(emote[0])
	if (server == None):
		return ''
	else:
		# Return the emote string if we can find it else return an empty string
		matches = [x for x in server.emojis if x.name==emote[1]]
		return matches[0] if matches else ''
