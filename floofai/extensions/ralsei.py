# Python Standard Library
import asyncio
import random

# Discord.py
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config
from floofai import emotes

# The category of Ralsei
class Ralsei(commands.Cog):
	"""All of the Ralsei-related commands!"""
	def __init__(self, client):
		self.client = client
		# Global used for party()
		self.activeParties = {}

	@commands.command()
	async def bakecake(self, ctx):
		'Floof.ai will bake you a nice cake!'
		await ctx.send('I\'ve baked you a cake! Enjoy! :cake: ')

	@commands.command()
	async def flirt(self, ctx):
		'Floof.ai will cutely try to flirt with you!'
		# List of possible flirts and embarrassments
		flirts = ['{}, your beauty is just... transcendant...',
			'{}, you make my heart flutter.',
			'{}, your hugs are the warmest.']
		embarrassments = ['Oh, oh dear! Wait!!',
			'I...I mean that as a friend of course...',
			'D-don\'t take that the wrong way >w<']
		# Send a random flirt
		await ctx.send(random.choice(flirts).format(ctx.message.author.mention))
		# Wait 4 seconds for comedic timing
		await asyncio.sleep(4)
		# Send a random embarrassment
		await ctx.send(random.choice(embarrassments))

	@commands.command()
	async def friends(self, ctx):
		'Floof.ai just wants to be friends!'
		await ctx.send('I hope we can be good friends!')

	@commands.command()
	async def goodmorning(self, ctx):
		'Floof.ai Tells You Good Morning'
		emote = emotes.getEmote(emotes.EmotesList.FloofGlad)
		await ctx.send('Good Morning! {}'.format(emote))

	@commands.command()
	async def goodnight(self, ctx):
		'Floof.ai Tells You Good Night'
		emote = emotes.getEmote(emotes.EmotesList.FloofGlad)
		await ctx.send('Good Night! {}'.format(emote))

	@commands.command()
	async def honey(self, ctx):
		'Floof.ai calls you honey!'
		await ctx.send('*Hugs you*   Hello, Honey! :heart: ')

	@commands.command()
	async def hug(self, ctx):
		'Floof.ai gives a great big hug!'
		await ctx.send('Hugs! :heart: \n ' \
			'https://i.imgur.com/vA8R8Ti.png \n')

	@commands.command()
	async def lewd(self, ctx):
		'No lewding the floof!'
		await ctx.send('...Please no. \n\n ')

	@commands.command()
	async def love(self, ctx):
		'Floof.ai loves all!'
		await ctx.send('Thank you! I love you too! :heart:')

	@commands.command()
	async def lullaby(self, ctx):
		'A link to Ralsei\'s lullaby on YouTube.'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=77Qrdyw48xw')

	@commands.command()
	async def magic(self, ctx):
		'Show Floof.ai magic!'
		await ctx.send(('Huh? You want to show me some magic...? '\
			'But that\'s just a mirror!'))
		await asyncio.sleep(3)
		await ctx.send('...')
		await asyncio.sleep(1)
		emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
		await ctx.send('Oh! Goodness! {}'.format(emote))

	@commands.command()
	async def moneyhug(self, ctx):
		'Floof.ai doesn\'t need money to want to hug you'
		await ctx.send('You don\'t need money ' \
			'to get me to hug you~')

	@commands.command()
	async def owo(self, ctx):
		'What\'s this?'
		await ctx.send('What\'s this? OwO')


	@commands.command()
	async def party(self, ctx):
		'Start a hug party! Woo!'
		channelid = ctx.message.channel.id

		# Check if the channel has an active party or not
		if channelid in self.activeParties:
			# Yes it does, so add the user to the list and return
			self.activeParties[channelid].append(ctx.message.author)
		else:
			# No it does not, so make a new party
			# Send the start message
			await ctx.send(':tada: WHOO! HUG PARTY! USE **{}party** TO JOIN!'\
				' IT WILL START IN ONE MINUTE! :tada:'.format(
				self.client.command_prefix))
			# Make a new list of users for the channel
			self.activeParties[channelid] = [ctx.message.author]
			# Wait 60 seconds
			await asyncio.sleep(60)
			# The party message template
			partymsg = ':tada: {} :tada:\n\nhttps://i.imgur.com/vA8R8Ti.png\n'\
				'I :heart: you all!'
			# Get mentions for all users in the list
			mentions = [user.mention for user in self.activeParties[channelid]]
			# Join them with ' :tada: ' between each message
			mentionblock = ' :tada: '.join(mentions)
			# Send the formatted template message
			await ctx.send(partymsg.format(mentionblock))
			# Delete the channel from self.activeParties
			del self.activeParties[channelid]

	@commands.command()
	async def pat(self, ctx):
		'Pat pat the Floof.ai'
		emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
		await ctx.send('Oh! Uhhh, Thanks! {}'.format(emote))

	@commands.command()
	async def police(self, ctx):
		'Floof.ai becomes the police!'
		await ctx.send(('PUT YOUR HANDS IN THE AIR! ' \
			'*Points stuffed animal*'))

	@commands.command(aliases=['PRAISE'])
	async def praise(self, ctx):
		'PRAISE THE FLOOF!'
		emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
		await ctx.send('P R A I S E  T H E  F L O O F {}'.format(emote))

	@commands.command()
	async def ruffle(self, ctx):
		'Ruffle Floof.ai\'s floof'
		await ctx.send('{} lovingly ruffled the floof.'.format(
												ctx.message.author.mention))
		await asyncio.sleep(2)
		emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
		await ctx.send('{} * Oh, haha... :heart: '.format(emote))

	@commands.command()
	async def tail(self, ctx):
		'No lewding of the floof!'
		await ctx.send('Don\'t lewd the floof')

	@commands.command()
	async def welcome(self, ctx):
		'Floof.ai says welcome!'
		await ctx.send('Welcome. I am the Prince of this Kingdom' \
			'\n\n...The KINGDOM OF DARKNESS.')

# Entry point for load_extension
def setup(client):
	client.add_cog(Ralsei(client))
