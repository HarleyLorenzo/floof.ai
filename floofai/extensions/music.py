# Discord.py
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config

class Music(commands.Cog):
	"""All music related commands!"""

	def __init__(self, client):
		self.client = client

	@commands.command()
	async def checker(self, ctx):
		'A link to Checker Dance on YouTube.'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=hmHq0o3blk0')

	@commands.command()
	async def dontforget(self, ctx):
		'A link to Don\'t Forget on YouTube'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=zMDSET0NIyM')

	@commands.command()
	async def fohad(self, ctx):
		'A link to Fields of Hopes and Dreams on YouTube'
		await ctx.send('https://www.youtube.com/' \
				'watch?v=qzBO0ZpgfRw')

	@commands.command()
	async def king(self, ctx):
		'A link to Chaos King on YouTube.'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=4sBhJP0VWy8')

	@commands.command()
	async def lancertheme(self, ctx):
		'A link to Lancer\'s Overworld Theme on YouTube'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=XucCwsKJSRA')

	@commands.command()
	async def rudebuster(self, ctx):
		'A link to Rude Buster on YouTube.'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=gUKnARapoWs')

	@commands.command()
	async def twr(self, ctx):
		'A link to The World Revolving on YouTube.'
		await ctx.send('https://www.youtube.com/' \
			'watch?v=52gdri1EyiA')

def setup(client):
	client.add_cog(Music(client))
