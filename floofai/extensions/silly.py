# Discord.py
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config
from floofai import emotes

class Silly(commands.Cog):
	"""All silly and fun commands!"""

	def __init__(self, client):
		self.client = client

	@commands.command(hidden=True)
	async def awoo(self, ctx):
		'The secret Wolfy command'
		await ctx.send('<@101822764100366336> ***AWOOOOOOOOOOOOOOOOOOOOOOO!***')

	@commands.command()
	async def beep(self, ctx):
		'Do a beep'
		await ctx.send('Boop!')

	@commands.command()
	async def blep(self, ctx):
		'Do a blep'
		await ctx.send(':stuck_out_tongue:')

	@commands.command()
	async def boop(self, ctx):
		'Do a boop'
		await ctx.send('Beep!')

	@commands.command()
	async def chaos(self, ctx):
		'Jevil does a nice dance.'
		await ctx.send('***CHAOS, CHAOS!*** \n\n ' \
			'https://i.imgur.com/DrZelZL.gif')

	@commands.command()
	async def defaultdance(self, ctx):
		'Floof.ai does the cursed Fortnite dance.'
		await ctx.send('https://i.imgur.com/Gj9L4Rq.gif')

	@commands.command(hidden=True)
	async def daffyandharley(self, ctx):
		'An emote of love'
		await ctx.send(':heart:')

	@commands.command()
	async def despacito(self, ctx):
		'A command only for true meme lords.'
		await ctx.send('\u0ca0_\u0ca0')

	@commands.command()
	async def egg(self, ctx):
		'\U0001f95a'
		await ctx.send(':egg:')

	@commands.command(hidden=True)
	async def harley(self, ctx):
		'The Ultra Secret Command'
		# Get the emote string
		emote = emotes.getEmote(emotes.EmotesList.harleyBooperSnoot)
		await ctx.send(str(emote))

	@commands.command(hidden=True)
	async def karma(self, ctx):
		'Karma is an egg'
		await ctx.send('Karma is an :egg: ')

	@commands.command()
	async def respect(self, ctx):
		'F'
		await ctx.send(':regional_indicator_f:')

# Entry Point for load_extension
def setup(client):
	client.add_cog(Silly(client))
