# Python Standard Library
import math
import random
import re
import time

# Discord.py
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config
from floofai import emotes
from floofai.config import USER, GUILD, CHANNEL, GLOBAL

class Utilities(commands.Cog):
	"""All utility commands!"""

	def __init__(self, client):
		self.client = client

	@commands.command()
	async def calc(self, ctx, *, expression:str):
		'Floof.ai does a math'
		calc_operators = {
			'+': {'func': (lambda a,b: a+b),  'prec':0, 'assoc':-1,
				  'digits': (lambda a,b: max(math.log(abs(a), 10),
											 math.log(abs(b), 10)) + 1)},
			'-': {'func': (lambda a,b: a-b),  'prec':0, 'assoc':-1,
				  'digits': (lambda a,b: max(math.log(abs(a), 10),
											 math.log(abs(b), 10)) + 1)},
			'*': {'func': (lambda a,b: a*b),  'prec':1, 'assoc':-1,
				  'digits': (lambda a,b: math.log(abs(a), 10) +
				math.log(abs(b), 10))},
			'/': {'func': (lambda a,b: a/b),  'prec':1, 'assoc':-1,
				  'digits': (lambda a,b: math.log(abs(a), 10))},
			'^': {'func': (lambda a,b: a**b), 'prec':2, 'assoc': 1,
				  'digits': (lambda a,b: math.log(abs(a), 10) * b + 1)},
		}

		def calc_numberize(numstr):
			try:
				return int(numstr)
			except:
				try:
					return float(numstr)
				except:
					return complex(numstr)

		# Useful regexes
		tokenReg = r'([\d.]+[ij]?|\(|\)|[a-zA-Z]+)'
		numberReg = r'^[\d.]+[ij]?$'

		# Tokenize
		expression = expression.replace(' ', '')
		tokens = re.split(tokenReg, expression)
		# Get rid of those pesky Nones and empty strings
		tokens = [x for x in tokens if x not in ['', None]]

		# Sanity check the tokens, and replace a couple constants
		for i, token in enumerate(tokens):
			if re.match(numberReg, token):
				continue
			if token in calc_operators:
				continue
			if token in '()':
				continue
			if token == 'e':
				tokens[i] = str(math.e)
				continue
			if token == 'pi':
				tokens[i] = str(math.pi)
				continue
			emote = emotes.getEmote(emotes.EmotesList.ralseiThink)
			await ctx.send('Are you messing with me? ' \
						  '{}'.format(emote))
			return

		# Infix to postfix (shunting yard algorithm)
		stack = []
		output = []
		# Loop through all tokens
		for token in tokens:
			# If a token is a number, numberize it and push to the stack
			if re.match(numberReg, token):
				token = calc_numberize(token.replace('i', 'j'))
				output.append(token)
			# If it's an operator, pop other higher precidence operators from the
			#  stack into output, then push it to the stack
			elif token in calc_operators:
				prec = calc_operators[token]['prec']
				assoc = calc_operators[token]['assoc']
				# This is gross don't look at it
				while stack and stack[-1] in calc_operators:
					stackOp = calc_operators[stack[-1]]
					if stackOp['prec'] > prec or \
						stackOp['prec'] == prec and stackOp['assoc'] == -1:
						output.append(stack.pop())
					else:
						break
				stack.append(token)
			# If token is an open bracket, push to the stack
			elif token == '(':
				stack.append(token)
			# If token is a close bracket, pop until the next open bracket
			#  then discard the close bracket
			elif token == ')':
				try:
					while stack[-1] != '(':
						output.append(stack.pop())
					stack.pop()
				except IndexError:
					emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
					await ctx.send('Uhm... you have mismatched brackets... ' \
								  '{}'.format(emote))
					return

		# Pop the rest of the stack to output
		while stack:
			token = stack.pop()
			if token != '(':
				output.append(token)

		# Evaluate the postfix expression
		stack = []
		# Loop through all tokens in the shunting yard output
		for token in output:
			# If it's a number, push to the stack
			if isinstance(token, (int, float, complex)):
				stack.append(token)
			# If it's an operator, check if the resulting digits is reasonable,
			#  then evaluate it and push the result to the stack
			else:
				op = calc_operators[token]
				b = stack.pop()
				a = stack.pop()
				if op['digits'](a, b) > 101:
					emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
					await ctx.send('Uhm... {}'.format(emote))
					return
				result = op['func'](a, b)
				stack.append(result)

		# Check for mismatched brackets
		if len(stack) > 1:
			emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
			await ctx.send('Uhm... you have mismatched brackets... ' \
						  '{}'.format(emote))
			return

		# Send along the result
		result = stack[-1]
		# Strip off parens and replace j with i
		result = str(result).replace('(', '').replace(')', '').replace('j', 'i')
		emote = emotes.getEmote(emotes.EmotesList.ralseiThink)
		await ctx.send('Hmmm... {} my calculator says:\n{}'.format(
																emote, result))

	@commands.command()
	async def invite(self, ctx):
		'Floof.ai\'s invite link so you can add him to your own server!'
		await ctx.send('Here is my invite link: \n \n ' \
			'https://discordapp.com/api/oauth2/authorize?client_id' \
			'={}&permissions=8&scope=bot'.format(config.client.user.id))

	@commands.command()
	async def ping(self, ctx):
		'Ping the bot!'
		# Send the message with formatted ms time
		await ctx.send('Pong! Time taken: **{:.0f} ms**'.format(
			config.client.latency * 1000))

	@commands.command()
	async def servers(self, ctx):
		'See how many servers Floof.ai is a part of!'
		await ctx.send('Floof.ai services {} servers!'.format(
													len(config.client.guilds)))

	@commands.command(aliases=['20hour'])
	async def twentyhour(self, ctx, offset:int=0):
		'Display the time in 20/100/100 format'
		# Get the current time in seconds
		# This is more precise than time.gmtime()
		now = (time.time() + (offset * 60 * 60)) % (24 * 60 * 60)
		# Perform the conversion from 24/60/60 to 20/100/100
		converted = now / .432
		# Format `converted` to be rounded and padded to 6 digits
		formatted = '{:06.0f}'.format(converted)
		# Split into 2 character groups and add colons between each group
		split = [formatted[x:x+2] for x in range(0, len(formatted), 2)]
		delimited = ':'.join(split)
		# Finally send it
		await ctx.send('The current time (in 20 hour time) is {}'.format(
																	delimited))

	@commands.command()
	async def version(self, ctx):
		'Display what version of Floof.ai the bot is running!'
		await ctx.send(config.version)

	@commands.command()
	async def remember(self, ctx, *, thing=None):
		'Remember a thing'
		if thing is None:
			thing = config.data[USER, ctx.message.author.id, 'remember']
			if thing is None:
				emote = emotes.getEmote(emotes.EmotesList.ralseiThink)
				await ctx.send('But... you didn\'t tell me to remember '\
							   'anything... {}'.format(emote))
			else:
				await ctx.send('You told me to remember:\n"{}"'.format(thing))
		else:
			config.data[USER, ctx.message.author.id, 'remember'] = thing
			config.data.save()
			emote = emotes.getEmote(emotes.EmotesList.ralseiLove)
			await ctx.send('Okay, I\'ll remember that!! {}'.format(emote))

	@commands.command()
	async def roll(self, ctx, *, dice):
		'Roll dice in XdY(+Z) form'
		# Check if it's valid
		if not re.match(r'(\d+)?d\d+([+\-]\d+)?', dice):
			emote = emotes.getEmote(emotes.EmotesList.ralseiThink)
			await ctx.send('Uhm.. that doesn\'t look like '\
						'XdY(+Z) format {}.'.format(emote))
			return

		# Separate out the offset, if it exists
		if '+' in dice:
			dice, offset = dice.split('+')
		elif '-' in dice:
			dice, offset = dice.split('-')
			offset = '-' + offset
		else:
			offset = 0

		# Separate the parameters of XdY format
		dicecount, dicesides = dice.split('d')

		# Integerize it all
		offset = int(offset)
		dicecount = int(dicecount) if dicecount else 1
		dicesides = int(dicesides)

		# Sanity checks
		if dicecount > 100:
			emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
			await ctx.send('That\'s a lot of dice. {}'.format(emote))
			return
		if dicesides > 1000:
			emote = emotes.getEmote(emotes.EmotesList.ralseiBlush)
			await ctx.send('No dice are that big. {}'.format(emote))
			return

		# Roll the dice
		rolls = [random.randint(1, dicesides) for x in range(dicecount)]

		# Format the result
		result = []

		if len(rolls) > 1:
			result.append('Rolls: ' + ', '.join([str(x) for x in rolls]) + '\n')
			# Sum
			result.append('Sum: {} {}'.format(sum(rolls),
				'({:+} = {})'.format(offset, sum(rolls) + offset)
					if offset else ''))
			# Max
			result.append('Max: {} {}'.format(max(rolls),
				'({:+} = {})'.format(offset, max(rolls) + offset)
					if offset else ''))
			# Min
			result.append('Min: {} {}'.format(min(rolls),
				'({:+} = {})'.format(offset, min(rolls) + offset)
					if offset else ''))
		else:
			result.append('Roll: {} {}'.format(rolls[0],
				'({:+} = {})'.format(offset, sum(rolls) + offset)
					if offset else ''))

		# Send it off to the user
		await ctx.send('\n'.join(result))

# Entry point for load_extension
def setup(client):
	# If loaded in as an extension delete the default help because
	# there is a help command overloaded inside the class
	client.add_cog(Utilities(client))
