# Discord.py
import discord
from discord.ext import commands

# Floof.ai Modules
from floofai import config

class Development(commands.Cog):
	"""All commands relating to the development of Floof.ai"""

	def __init__(self, client):
		self.client = client

	@commands.command()
	async def discord(self, ctx):
		'The invite link to Floof.ai\'s home server: Church of Ralsei!'
		await ctx.send('Join our discord! It is both a fun ' \
			'place to hang out and talk about Deltarune and also contains ' \
			'Floof.ai development announcements, support and development! \n\n ' \
			'https://discord.gg/Sm8fV3E ')

	@commands.command()
	async def donate(self, ctx):
		'Donation links and instructions if you want to support Floof.ai!'
		await ctx.send('If you would like to support the project '\
			'and its server costs, you can donate to Harley A.W. Lorenzo on ' \
			'Ko-fi at https://ko-fi.com/T6T1Q4NN \n\n' \
			'Additionally you can donate bitcoin to the ' \
			'address: 1NFPKPivsSSrR9WZgq5qoPSPMfY3tdfB76')

	@commands.command()
	async def gitlab(self, ctx):
		'A link to Floof.ai\'s Gitlab repository!'
		await ctx.send('https://gitlab.com/HarleyLorenzo/floof.ai')

# Entry point for load_extension
def setup(client):
	client.add_cog(Development(client))
