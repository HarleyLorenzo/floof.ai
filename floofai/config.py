### Package globals

from discord.ext import commands
from floofai.dicttree import Dicttree

# Version number
version = '3.3.1'
# Bot prefix and also setting for the help message to always pm
client = commands.Bot(command_prefix='f.',
    help_command=commands.DefaultHelpCommand(dm_help=True))

# Dicttree for user data
USER = 'user'
CHANNEL = 'channel'
GUILD = 'guild'
GLOBAL = 'global'
data = Dicttree('userdata')
