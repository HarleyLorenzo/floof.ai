import pickle as _pickle
import sys as _sys

class Dicttree():
	def __init__(self, path=None, verbose=False):
		self._path = path
		self._dict = {}
		if path is not None:
			try:
				self.load()
			except Exception as e:
				if verbose:
					print('Error while loading "{}":\n{}'.format(path, e),
															file=_sys.stderr)

	def _traverse(self, names):
		'''
			Traverse _dict from the root to the last item in `names`,
			creating empty dicts when a child does not exist.
			Then return the leaf.
		'''

		parent = self._dict
		for name in names:
			if name not in parent or not isinstance(parent[name], dict):
				parent[name] = {}
			parent = parent[name]

		return parent
		
	def __getitem__(self, names):
		# Ensure `names` is always a tuple.
		if not isinstance(names, tuple):
			names = (names,)

		# Traverse to the second to last node
		parent = self._traverse(names[:-1])

		# If the last node does not exist, set it to None
		if names[-1] not in parent:
			parent[names[-1]] = None
		
		return parent[names[-1]]

	def __setitem__(self, names, value):
		# Ensure `names` is always a tuple.
		if not isinstance(names, tuple):
			names = (names,)

		# Traverse to the second to last node
		parent = self._traverse(names[:-1])

		# Set the last node to `value`
		parent[names[-1]] = value

	def __delitem__(self, names):
		# Ensure `names` is always a tuple.
		if not isinstance(names, tuple):
			names = (names,)

		# Traverse to the second to last node
		parent = self._traverse(names[:-1])

		# Delete the last node
		del parent[names[-1]]
		
	def __repr__(self):
		return repr(self._dict)

	def __iter__(self):
		return self._dict.__iter__()

	def save(self, path=None):
		'Save the internal dict to a file'
		path = path if path else self._path
		if path is None:
			raise AttributeError('Path required')
		with open(path, 'wb') as f:
			f.write(_pickle.dumps(self._dict))
	
	def load(self, path=None):
		'Load a file and store in the internal dict'
		path = path if path else self._path
		if path is None:
			raise AttributeError('Path required')
		with open(path, 'rb') as f:
			self._dict = _pickle.loads(f.read())
