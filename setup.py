from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md')) as f:
	long_description = f.read()

setup(
	name="Floof.ai",
	packages=["floofai"],
	version="3.3.1",
	description="The Floof.ai discord bot, the cutest bot around.",
	long_description=long_description,
	url='https://gitlab.com/HarleyLorenzo/floof.ai',
	author='Harley A.W. Lorenzo',
	author_email='hl1998@protonmail.com',

	classifiers=[
		'Development Status :: 5 - Production/Stable',
		'Natural Language :: English',
		('License :: OSI Approved :: ' \
			'GNU General Public License v3 (GPLv3)'),
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3.5',
		'Programming Language :: Python :: 3.6',
	],

	entry_points={
		'console_scripts':[
			'floofai = floofai.floofai:main',
		],
	},

	py_modules=[
		'floofai.dicttree',
		'floofai.extensions.development',
		'floofai.extensions.music',
		'floofai.extensions.ralsei',
		'floofai.extensions.silly',
		'floofai.extensions.utilities',
	],

	data_files=[('', ['cfg/systemd/floofai.service']),
	],

	install_requires=['discord.py'],

	python_requires='>=3.5, !=3.7.*',

	project_urls={
		'Source': 'https://github.com/Harley/floof.ai/',
		'Bug Reports':
			'https://gitlab.com/HarleyLorenzo/floof.ai/issues',
	},
)
